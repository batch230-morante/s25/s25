console.log("Hello World");

// [SECTION] JSON Objects
/*
    - JSON stands for JavaScript Object Notation
    - JSON is also used in other programming languages
    - Core JavaScript has a built in JSON Objects that contains methods for passing JSON Objects
    - JSON is used for serializing different data types into bytes
*/


// JSON Object 
/*
    JSON also uses the "key/value pairs" just like the object properties in JavaScript
    - Key/Property names should be enclosed with double quotes

    Syntax:
    {
        "propertyA" : "valueA",
        "propertyB" : "valueB"
    }
*/

/*
{
    "city" : "Quezon City",
    "province" : "Metro Manila",
    "country" : "Philippines"
}
*/

// JSON Arrays
// Arrays in JSON are almost same as arrays in JavaScript
// Arrays in JSON Object
/*
    "cities" : [
        {
            "city" : "Quezon City",
            "province" : "Metro Manila",
            "country" : "Philippines"
        },
        {
            "city" : "Manila City",
            "province" : "Metro Manila",
            "country" : "Philippines"
        },
        {
            "city" : "Makati City",
            "province" : "Metro Manila",
            "coutry" : "Philippines"
        }
    ]

*/

// [SECTION] JSON Methods
// The "JSON Object" contains methods for prsing and converting data in stringified JSON
// The JSON data is sent or received in text-only(String) format

// Converting data into stringified JSON

let batchasArr = [
    {
        batchNAme: 230,
        schedule: "Part Time"
    },
    {
        batchNAme: 240,
        schedule: "Full Time"
    }
];

console.log(batchasArr);

console.log("Result from stringified method: ");

/* Syntax: JSON.stringify(arrayName/objectName); */

console.log(JSON.stringify(batchasArr));


let data = JSON.stringify(
    {
        name: "John",
        age: 31,
        address: {
            city: "Manila",
            country: "Philippines"
        }
    }
);

console.log(data);

/* 

// User Details
let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password: ");



// firstname w/ value
let user = JSON.stringify(
    {
        name: firstName,
        lastname: lastName,
        email: email,
        password: password
    }
);

console.log(user);
*/
// [SECTION] Covnerting Stringified JSON into JavaScript Objects:

let batchesJSON = `[
    {
        "batchNAme" : 230,
        "schedule" : "Part Time"
    },
    {
        "batchNAme" : 240,
        "schedule" : "Full Time"
    }
]`;

console.log("batchesJSON content: ");
console.log(batchesJSON);

console.log("Result from parse method: ");
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchNAme);

let stringifiedObject = `
    {
        "name" : "John",
        "age" : 31,
        "address" : {
            "city" : "Manila",
            "country" : "Philippines"
        }
    }
`

console.log(stringifiedObject);
let parsedStringObj = JSON.parse(stringifiedObject);

console.log(parsedStringObj);

console.log(JSON.parse(stringifiedObject));